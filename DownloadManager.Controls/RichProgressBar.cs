﻿using System.Windows;
using System.Windows.Controls;

namespace DownloadManager.Controls
{
    public class RichProgressBar : ProgressBar
    {
        static RichProgressBar()
        {
            DefaultStyleKeyProperty.OverrideMetadata(typeof(RichProgressBar), new FrameworkPropertyMetadata(typeof(RichProgressBar)));
        }

        public string OperationName
        {
            get => (string)GetValue(OperationNameProperty);
            set => SetValue(OperationNameProperty, value);
        }
        public string Description
        {
            get => (string)GetValue(DescriptionProperty);
            set => SetValue(DescriptionProperty, value);
        }

        public static readonly DependencyProperty OperationNameProperty = DependencyProperty.Register("OperationName", typeof(string), typeof(RichProgressBar));
        public static readonly DependencyProperty DescriptionProperty = DependencyProperty.Register("Description", typeof(string), typeof(RichProgressBar));
    }
}
