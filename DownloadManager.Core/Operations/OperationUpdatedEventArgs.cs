﻿using System;
using DownloadManager.Core.Interfaces;

namespace DownloadManager.Core.Operations
{
    public class OperationUpdatedEventArgs : EventArgs
    {
        public IOperation Operation { get; }

        public OperationUpdatedEventArgs(IOperation operation)
        {
            Operation = operation;
        }
    }
}