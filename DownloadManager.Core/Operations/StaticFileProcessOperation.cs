﻿namespace DownloadManager.Core.Operations
{
    public class StaticFileProcessOperation : FileProcessOperation
    {
        public StaticFileProcessOperation(string name, string description, int startPosition, int total, Action action)
            : base(name, description, startPosition, total, action, true)
        {
        }
    }
}