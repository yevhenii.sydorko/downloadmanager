﻿using DownloadManager.Core.Units;

namespace DownloadManager.Core.Operations
{
    public class FileProgresslessOperation : Operation
    {
        public FileProgresslessOperation(string name, string description, Action action)
            : base(false, name, description, 0, 0, action, new BytesStrategy(), true)
        {
        }

        public FileProgresslessOperation(string name, string description, int startPosition, int total, Action action)
            : base(false, name, description, startPosition, total, action, new BytesStrategy(), true)
        {
        }
        public FileProgresslessOperation(string name, string description, int total, Action action)
            : base(false, name, description, total, total, action, new BytesStrategy(), true)
        {
        }
    }
}