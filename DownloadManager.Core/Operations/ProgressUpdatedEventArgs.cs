﻿using System;

namespace DownloadManager.Core.Operations
{
    public class ProgressUpdatedEventArgs : EventArgs
    {
        public int Progress { get; }

        public ProgressUpdatedEventArgs(int progress)
        {
            Progress = progress;
        }
    }
}