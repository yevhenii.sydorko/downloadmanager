﻿using DownloadManager.Core.Interfaces;
using DownloadManager.Core.Units;

namespace DownloadManager.Core.Operations
{
    public class Operation : IOperation
    {
        private readonly IUnitStrategy<int> _unitStrategy;

        public Operation(bool hasProgress, string name, string description, int startPosition, int total, Action action, IUnitStrategy<int> unitStrategy, bool isStatic)
        {
            _unitStrategy = unitStrategy;
            IsStatic = isStatic;
            StartPosition = startPosition;
            HasProgress = hasProgress;
            Total = total;
            Name = name;
            Description = description;
            PossibleAction = action;
        }
        
        public Action PossibleAction { get; }
        public int StartPosition { get; }
        public string Name { get; }
        public string Description { get; }
        public int Total { get; }
        public bool HasProgress { get; }
        public bool IsStatic { get; }

        public virtual string FormatValue(int value)
        {
            return _unitStrategy.GetUnitValue(value);
        }
    }
}