﻿namespace DownloadManager.Core.Operations
{
    public class OperationData
    {
        public OperationData(string name, string description, string actionName)
        {
            Name = name;
            Description = description;
            ActionName = actionName;
        }

        public string Name { get; }
        public string Description { get; }
        public string ActionName { get; }
    }
}