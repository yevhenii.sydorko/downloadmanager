﻿using DownloadManager.Core.Units;

namespace DownloadManager.Core.Operations
{
    public class FileProcessOperation : Operation
    {
        public FileProcessOperation(string name, string description, int startPosition, int total, Action action) 
            : this(name, description, startPosition, total, action, false)
        {
        }

        public FileProcessOperation(string name, string description, int startPosition, int total, Action action, bool isStatic)
            : base(true, name, description, startPosition, total, action, new BytesStrategy(), isStatic)
        {
        }
    }
}