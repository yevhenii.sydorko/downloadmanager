﻿using System;
using System.Threading;
using System.Threading.Tasks;
using DownloadManager.Core.Interfaces;
using DownloadManager.Core.Operations;
using DownloadMaster.Network;

namespace DownloadManager.Core
{
    public class DownloadProvider : IOperationProvider
    {
        private readonly IDownloader _downloader;
        private readonly IDownloadOperationDataStore _dataStore;
        private CancellationTokenSource? _cts;
        private int _currentPosition;

        public DownloadProvider(IDownloader downloader, IDownloadOperationDataStore dataStore)
        {
            _downloader = downloader;
            _dataStore = dataStore;
            _downloader.Connecting += DownloaderConnecting;
            _downloader.Connected += DownloaderConnected;
            _downloader.BytesReceived += DownloaderBytesReceived;
            _downloader.Finishing += DownloaderFinishing;
            _downloader.Finished += DownloaderFinished;
            _downloader.Paused += DownloaderPaused;
        }

        public Task StartAsync()
        {
            _currentPosition = 0;
            return RunAsync(_currentPosition);
        }

        public void Pause()
        {
            _cts?.Cancel();
        }

        public Task ResumeAsync()
        {
            return RunAsync(_currentPosition);
        }

        private Task RunAsync(int progress)
        {
            if (_cts != null && !_cts.IsCancellationRequested)
            {
                throw new InvalidOperationException("Operation is already started.");
            }

            _cts = new CancellationTokenSource();
            return Task.Run(() => { _downloader.StartDownload(_cts.Token, progress); });
        }

        private void DownloaderPaused()
        {
            var data = _dataStore.GetData(DownloadStep.Paused);
            var action = new Action(data.ActionName, ActionType.Continue);
            OnOperationUpdated(new StaticFileProcessOperation(data.Name, data.Description, _currentPosition, _downloader.TotalBytesToDownload, action));
        }

        private void DownloaderConnecting()
        {
            var data = _dataStore.GetData(DownloadStep.Connecting);
            var action = new Action(data.ActionName, ActionType.Pause);
            OnOperationUpdated(new FileProgresslessOperation(data.Name, data.Description, action));
        }

        private void DownloaderFinished()
        {
            _cts?.Dispose();
            _cts = null;
            var data = _dataStore.GetData(DownloadStep.Finished);
            var action = new Action(data.ActionName, ActionType.Restart);
            OnOperationUpdated(new FileProgresslessOperation(data.Name, data.Description, _downloader.TotalBytesToDownload, action));
        }

        private void DownloaderFinishing()
        {
            var data = _dataStore.GetData(DownloadStep.Finishing);
            var action = new Action(data.ActionName, ActionType.Pause);
            OnOperationUpdated(new FileProgresslessOperation(data.Name, data.Description, _downloader.TotalBytesToDownload, action));
        }

        private void DownloaderBytesReceived(int progress)
        {
            _currentPosition += progress;
            OnProgressUpdated(progress);
        }

        private void DownloaderConnected()
        {
            var data = _dataStore.GetData(DownloadStep.Downloading);
            var action = new Action(data.ActionName, ActionType.Pause);
            OnOperationUpdated(new FileProcessOperation(data.Name, data.Description, _currentPosition, _downloader.TotalBytesToDownload, action));
        }

        private void OnOperationUpdated(IOperation operation)
        {
            OperationUpdated?.Invoke(this, new OperationUpdatedEventArgs(operation));
        }

        private void OnProgressUpdated(int progress)
        {
            ProgressUpdated?.Invoke(this, new ProgressUpdatedEventArgs(progress));
        }

        public event EventHandler<OperationUpdatedEventArgs>? OperationUpdated;
        public event EventHandler<ProgressUpdatedEventArgs>? ProgressUpdated;
    }
}
