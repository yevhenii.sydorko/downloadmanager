﻿namespace DownloadManager.Core.Interfaces
{
    public interface IOperation
    {
        string FormatValue(int value);
        string Name { get; }
        string Description { get; }
        Action PossibleAction { get; }
        int StartPosition { get; }
        int Total { get; }
        bool HasProgress { get; }
        bool IsStatic { get; }
    }
}
