﻿using System;
using System.Threading.Tasks;
using DownloadManager.Core.Operations;

namespace DownloadManager.Core.Interfaces
{
    public interface IOperationProvider
    {
        Task StartAsync();
        void Pause();
        Task ResumeAsync();
        event EventHandler<OperationUpdatedEventArgs> OperationUpdated;
        event EventHandler<ProgressUpdatedEventArgs> ProgressUpdated;
    }
}