﻿using DownloadManager.Core.Operations;

namespace DownloadManager.Core.Interfaces
{
    public interface IDownloadOperationDataStore
    {
        OperationData GetData(DownloadStep step);
    }
}