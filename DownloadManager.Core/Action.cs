﻿namespace DownloadManager.Core
{
    public class Action
    {
        public Action(string name, ActionType type)
        {
            Name = name;
            Type = type;
        }

        public string Name { get; }
        public ActionType Type { get; }
    }
}