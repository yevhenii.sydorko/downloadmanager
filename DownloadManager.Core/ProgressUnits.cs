﻿namespace DownloadManager.Core
{
    public enum ProgressUnits
    {
        None,
        Bytes,
        Files
    }
}