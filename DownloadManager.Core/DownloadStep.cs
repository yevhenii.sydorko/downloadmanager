﻿namespace DownloadManager.Core
{
    public enum DownloadStep
    {
        Connecting,
        Downloading,
        Finishing,
        Finished,
        Paused
    }
}