﻿namespace DownloadManager.Core
{
    public enum ActionType
    {
        Start,
        Stop,
        Continue,
        Restart,
        Pause
    }
}