﻿using System;

namespace DownloadManager.Core.Units
{
    public static class IntExtensions
    {
        private static readonly string[] SizeSuffixes = { "b", "kb", "mb", "gb", "tb", "pb", "eb", "zb", "yb" };
        
        public static string Format(this int value, int decimalPlaces = 1)
        {
            if (value < 0)
            {
                return "-" + (-value).Format();
            }

            if (value == 0)
            {
                return string.Format("{0:n" + decimalPlaces + "}b", 0);
            }

            int mag = (int)Math.Log(value, 1024);

            decimal adjustedSize = (decimal)value / (1L << (mag * 10));

            if (Math.Round(adjustedSize, decimalPlaces) >= 1000)
            {
                mag += 1;
                adjustedSize /= 1024;
            }

            return string.Format("{0:n" + decimalPlaces + "}{1}", adjustedSize, SizeSuffixes[mag]);
        }
    }
}
