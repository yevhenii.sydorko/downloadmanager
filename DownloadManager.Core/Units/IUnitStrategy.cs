﻿namespace DownloadManager.Core.Units
{
    public interface IUnitStrategy<in T>
    {
        string GetUnitValue(T input);
    }
}