﻿namespace DownloadManager.Core.Units
{
    public class BytesStrategy : IUnitStrategy<int>
    {
        public string GetUnitValue(int input)
        {
            return input.Format();
        }
    }
}
