﻿using System;

namespace DownloadManager
{
    public class ProgressStatus
    {
        public ProgressStatus(int progress, double speed, TimeSpan timeLeft)
        {
            Progress = progress;
            Speed = speed;
            TimeLeft = timeLeft;
        }

        public int Progress { get; }
        public double Speed { get; }
        public TimeSpan TimeLeft { get; }
    }
}