﻿using System;
using System.Collections.Generic;
using System.Timers;

namespace DownloadManager
{
    public struct ProgressValue
    {
        public ProgressValue(DateTime dateTime, int value)
        {
            DateTime = dateTime;
            Value = value;
        }

        public DateTime DateTime { get; }
        public int Value { get; }
    }

    public class EstimatedTimeCalculator : IEstimatedTimeCalculator
    {
        private Timer _timer;
        private int _total;
        private DateTime _startTime;
        public event Action<ProgressStatus> ProgressStatusUpdated;
        private readonly Queue<ProgressValue> _progressValues = new Queue<ProgressValue>();
        private ProgressValue _lastValue;
        private readonly TimeSpan _calculationInterval = TimeSpan.FromSeconds(3);
        private readonly object _locker = new object();
        private int _timerInterval = 500;

        public void Start(int startPosition, int total)
        {
            _startTime = DateTime.UtcNow;
            _lastValue = new ProgressValue(DateTime.MinValue, startPosition);
            _progressValues.Clear();
            _total = total;
            _timer = new Timer { Interval = _timerInterval };
            _timer.Elapsed += TimerElapsed;
            _timer.Start();
        }

        public void Stop()
        {
            if (_timer != null)
            {
                _timer.Elapsed -= TimerElapsed;
                _timer.Stop();
            }
        }

        public void ProgressUpdated(int progress)
        {
            lock (_locker)
            {
                _lastValue = new ProgressValue(DateTime.UtcNow, _lastValue.Value + progress);
                _progressValues.Enqueue(_lastValue);
            }
        }

        private void TimerElapsed(object sender, ElapsedEventArgs e)
        {
            int overallProgress;
            double speed;
            lock (_locker)
            {
                var timeLimit = DateTime.UtcNow - _calculationInterval;
                int firstValue = 0;
                var firstDateTime = DateTime.MinValue;
                while (_progressValues.Count > 0 && firstDateTime < timeLimit)
                {
                    var value = _progressValues.Dequeue();
                    firstValue = value.Value;
                    firstDateTime = value.DateTime;
                }

                speed = (_lastValue.Value - firstValue) / (_lastValue.DateTime - firstDateTime).TotalSeconds;
                overallProgress = _lastValue.Value;
            }

            var totalSeconds = overallProgress == 0 ? 0 : (DateTime.UtcNow - _startTime).TotalSeconds / overallProgress * (_total - overallProgress);
            TimeSpan time = TimeSpan.FromSeconds(totalSeconds);
            ProgressStatusUpdated?.Invoke(new ProgressStatus(overallProgress, speed, time));
        }
    }
}