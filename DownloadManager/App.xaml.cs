﻿using System;
using System.Windows;
using DownloadManager.Core;
using DownloadManager.Core.Interfaces;
using DownloadManager.ViewModels;
using DownloadMaster.Network;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;

namespace DownloadManager
{
    public partial class App : Application
    {
        private readonly IHost _host;

        public App()
        {
            _host = Host.CreateDefaultBuilder().ConfigureServices((context, services) =>
                {
                    ConfigureServices(context.Configuration, services);
                }).Build();
        }

        private void ConfigureServices(IConfiguration configuration, IServiceCollection services)
        {
            services.AddSingleton<DownloadMe>();
            services.AddScoped<IDownloadOperationDataStore, DownloadOperationDataStore>();
            services.AddScoped<IDownloader, Downloader>();
            services.AddScoped<IOperationProvider, DownloadProvider>();
            services.AddScoped<IEstimatedTimeCalculator, EstimatedTimeCalculator>();
            services.AddSingleton<MainViewModel>();
            services.AddSingleton<MainWindow>();
        }

        protected override async void OnStartup(StartupEventArgs e)
        {
            await _host.StartAsync();

            var mainWindow = _host.Services.GetRequiredService<MainWindow>();
            mainWindow.Show();

            base.OnStartup(e);
        }

        protected override async void OnExit(ExitEventArgs e)
        {
            using (_host)
            {
                await _host.StopAsync(TimeSpan.FromSeconds(5));
            }

            base.OnExit(e);
        }
    }
}
