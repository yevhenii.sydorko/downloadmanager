﻿
namespace DownloadManager.Infrastructure
{
    public static class TaskExtensions
    {
        public static async void SafeFireAndForget(this System.Threading.Tasks.Task task, bool continueOnCapturedContext = true, System.Action<System.Exception> onException = null)
        {
            try
            {
                await task.ConfigureAwait(continueOnCapturedContext);
            }
            catch (System.Exception ex) when (onException != null)
            {
                onException?.Invoke(ex);
            }
        }
    }
}
