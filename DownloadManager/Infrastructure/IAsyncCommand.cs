﻿using System.Threading.Tasks;
using System.Windows.Input;

namespace DownloadManager.Infrastructure
{
    public interface IAsyncCommand : ICommand
    {
        Task ExecuteAsync(object parameter);
        bool CanExecute();
    }
}