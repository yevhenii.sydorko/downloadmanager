﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;

namespace DownloadManager.Infrastructure
{
    public class AsyncCommand : IAsyncCommand
    {
        public event EventHandler CanExecuteChanged;

        private bool _isExecuting;
        private readonly Func<object, Task> _execute;
        private readonly Func<bool> _canExecute;
        private readonly Action<Exception> _onException;
        private readonly bool _continueOnCapturedContext;

        public AsyncCommand(
            Func<object, Task> execute,
            Func<bool> canExecute = null,
            Action<Exception> onException = null,
            bool continueOnCapturedContext = true)
        {
            _execute = execute;
            _canExecute = canExecute;
            _onException = onException;
            _continueOnCapturedContext = continueOnCapturedContext;
        }

        public bool CanExecute()
        {
            return !_isExecuting && (_canExecute?.Invoke() ?? true);
        }

        public async Task ExecuteAsync(object parameter)
        {
            if (CanExecute())
            {
                try
                {
                    _isExecuting = true;
                    await _execute(parameter);
                }
                finally
                {
                    _isExecuting = false;
                }
            }

            RaiseCanExecuteChanged();
        }

        public void RaiseCanExecuteChanged()
        {
            CanExecuteChanged?.Invoke(this, EventArgs.Empty);
        }

        #region Explicit implementations

        bool ICommand.CanExecute(object parameter)
        {
            return CanExecute();
        }

        void ICommand.Execute(object parameter)
        {
            ExecuteAsync(parameter).SafeFireAndForget(_continueOnCapturedContext, _onException);
        }
        #endregion

        public static void UpdateCanExecute()
        {
            CommandManager.InvalidateRequerySuggested();
        }

    }
}
