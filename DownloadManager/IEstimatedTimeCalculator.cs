﻿using System;

namespace DownloadManager
{
    public interface IEstimatedTimeCalculator
    {
        event Action<ProgressStatus> ProgressStatusUpdated;
        void Start(int startPosition, int total);

        void Stop();

        void ProgressUpdated(int progress);
    }
}