﻿using DownloadManager.Core.Interfaces;

namespace DownloadManager.ViewModels
{
    public class MainViewModel : BaseViewModel
    {
        public ProgressViewModel Progress { get; }

        public MainViewModel(IOperationProvider operationProvider, IEstimatedTimeCalculator estimatedTimeCalculator)
        {
            Progress = new ProgressViewModel(operationProvider, estimatedTimeCalculator);
        }
    }
}
