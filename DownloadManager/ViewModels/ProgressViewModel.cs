﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using DownloadManager.Core;
using DownloadManager.Core.Interfaces;
using DownloadManager.Core.Operations;
using DownloadManager.Infrastructure;

namespace DownloadManager.ViewModels
{
    public class ProgressViewModel : BaseViewModel
    {
        private readonly IEstimatedTimeCalculator _estimatedTimeCalculator;
        private string _actionText;
        private OperationViewModel _operation;
        private readonly IOperationProvider _operationProvider;
        private readonly ICommand _startCommand;
        private readonly ICommand _pauseCommand;
        private readonly ICommand _resumeCommand;
        private ICommand _actionCommand;

        public ProgressViewModel(IOperationProvider operationProvider, IEstimatedTimeCalculator estimatedTimeCalculator)
        {
            _estimatedTimeCalculator = estimatedTimeCalculator;
            _operationProvider = operationProvider;
            _operationProvider.OperationUpdated += OperationProviderOperationUpdated;
            _startCommand = new AsyncCommand(o => StartAsync());
            _pauseCommand = new RelayCommand(o => Pause(), t => true);
            _resumeCommand = new AsyncCommand(o => ResumeAsync());
            ActionText = "START";
            ActionCommand = _startCommand;
        }

        public ICommand ActionCommand
        {
            get => _actionCommand;
            set
            {
                _actionCommand = value;
                OnPropertyChanged();
            }
        }

        public string ActionText
        {
            get => _actionText;
            set
            {
                _actionText = value;
                OnPropertyChanged();
            }
        }

        public OperationViewModel Operation
        {
            get => _operation;
            set
            {
                _operation = value;
                OnPropertyChanged();
            }
        }

        private Task StartAsync()
        {
            return _operationProvider.StartAsync();
        }

        private void Pause()
        {
            _operationProvider.Pause();
        }

        private Task ResumeAsync()
        {
            return _operationProvider.ResumeAsync();
        }

        private void StartOperation(IOperation operation)
        {
            ActionText = operation.PossibleAction.Name.ToUpperInvariant();
            SetAction(operation);
            Operation?.Stop();
            Operation = new OperationViewModel(operation, _operationProvider, _estimatedTimeCalculator);
        }

        private void SetAction(IOperation operation)
        {
            switch (operation.PossibleAction.Type)
            {
                case ActionType.Start:
                    ActionCommand = _startCommand;
                    break;
                case ActionType.Stop:
                    ActionCommand = _pauseCommand;
                    break;
                case ActionType.Continue:
                    ActionCommand = _resumeCommand;
                    break;
                case ActionType.Restart:
                    ActionCommand = _startCommand;
                    break;
                case ActionType.Pause:
                    ActionCommand = _pauseCommand;
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        private void OperationProviderOperationUpdated(object sender, OperationUpdatedEventArgs e)
        {
            StartOperation(e.Operation);
        }
    }
}
