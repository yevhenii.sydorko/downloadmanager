﻿using DownloadManager.Core.Interfaces;
using DownloadManager.Core.Operations;
using DownloadManager.Core.Units;

namespace DownloadManager.ViewModels
{
    public class OperationViewModel : BaseViewModel
    {
        private readonly IOperation _operation;
        private readonly IEstimatedTimeCalculator _estimatedTimeCalculator;
        private string _operationName;
        private string _description;
        private int _progress;
        private int _maxProgress;
        private readonly IOperationProvider _operationProvider;
        private bool _progressless;

        public OperationViewModel(IOperation operation, IOperationProvider operationProvider,
            IEstimatedTimeCalculator estimatedTimeCalculator)
        {
            _operation = operation;
            _estimatedTimeCalculator = estimatedTimeCalculator;
            _estimatedTimeCalculator.ProgressStatusUpdated += CalculatorProgressStatusUpdated;

            _operationProvider = operationProvider;
            _operationProvider.ProgressUpdated += OperationProviderProgressUpdated;
            OperationName = operation.Name;
            MaxProgress = operation.Total;
            Progressless = !operation.HasProgress;
            Progress = operation.StartPosition;
            if (operation.IsStatic)
            {
                if (string.IsNullOrEmpty(operation.Description))
                {
                    Description = string.Empty;
                }
                else if (!operation.HasProgress)
                {
                    Description = $"{operation.Description} {operation.FormatValue(MaxProgress)}";
                }
                else
                {
                    Description = $"{_operation.Description} {_operation.FormatValue(Progress)}/{_operation.FormatValue(MaxProgress)}";
                }
            }
            else
            {
                _estimatedTimeCalculator.Start(operation.StartPosition, operation.Total);
            }
        }

        public bool Progressless
        {
            get => _progressless;
            set
            {
                _progressless = value;
                OnPropertyChanged();
            }
        }

        public string OperationName
        {
            get => _operationName;
            set
            {
                _operationName = value;
                OnPropertyChanged();
            }
        }

        public string Description
        {
            get => _description;
            set
            {
                _description = value;
                OnPropertyChanged();
            }
        }

        public int Progress
        {
            get => _progress;
            set
            {
                _progress = value;
                OnPropertyChanged();
            }
        }

        public int MaxProgress
        {
            get => _maxProgress;
            set
            {
                _maxProgress = value;
                OnPropertyChanged();
            }
        }

        private void CalculatorProgressStatusUpdated(ProgressStatus obj)
        {
            Progress = obj.Progress;
            Description = $"{_operation.Description} {_operation.FormatValue(Progress)}/{_operation.FormatValue(MaxProgress)}, speed {((int)obj.Speed).Format()}/s, time {obj.TimeLeft:hh\\:mm\\:ss}";
        }

        private void OperationProviderProgressUpdated(object sender, ProgressUpdatedEventArgs e)
        {
            _estimatedTimeCalculator.ProgressUpdated(e.Progress);
        }

        public void Stop()
        {
            _estimatedTimeCalculator.Stop();
            _estimatedTimeCalculator.ProgressStatusUpdated -= CalculatorProgressStatusUpdated;
            _operationProvider.ProgressUpdated -= OperationProviderProgressUpdated;
        }
    }
}