﻿namespace DownloadManager.ViewModels
{
    public class ProgressStep
    {
        public ProgressStep(string name, string description)
        {
            Name = name;
            Description = description;
        }

        public string Name { get; }
        public string Description { get; }
    }
}