﻿using System.Collections.Generic;
using DownloadManager.Core;
using DownloadManager.Core.Interfaces;
using DownloadManager.Core.Operations;

namespace DownloadManager
{
    public class DownloadOperationDataStore : IDownloadOperationDataStore
    {
        private static readonly Dictionary<DownloadStep, OperationData> _mapper = new Dictionary<DownloadStep, OperationData>
        {
            {DownloadStep.Connecting, new OperationData("Connecting...", string.Empty,"Pause")},
            {DownloadStep.Downloading, new OperationData("Downloading...", "Downloaded","Pause")},
            {DownloadStep.Finishing, new OperationData("Finishing...", "Downloaded","Pause")},
            {DownloadStep.Finished, new OperationData("Finished!", "Downloaded","Restart")},
            {DownloadStep.Paused, new OperationData("Paused", "Downloaded","Continue")}
        };
        public OperationData GetData(DownloadStep step)
        {
            return _mapper[step];
        }
    }
}