﻿using System;
using System.Threading;

namespace DownloadMaster.Network
{
    public interface IDownloader
    {
        event Action<int>? BytesReceived;
        event Action? Connecting;
        event Action? Connected;
        event Action? Finishing;
        event Action? Finished;
        event Action? Paused;
        int TotalBytesToDownload { get; }
        void StartDownload(CancellationToken cancellationToken, int initialPosition = 0);
    }
}