﻿using System;
using System.Threading;

namespace DownloadMaster.Network
{
    public class Downloader : IDownloader
    {
        private readonly DownloadMe _downloadMe;

        public Downloader(DownloadMe downloadMe)
        {
            _downloadMe = downloadMe;
        }

        public event Action<int>? BytesReceived
        {
            add => _downloadMe.BytesReceived += value;
            remove => _downloadMe.BytesReceived -= value;
        }

        public event Action? Connecting
        {
            add => _downloadMe.Connecting += value;
            remove => _downloadMe.Connecting -= value;
        }

        public event Action? Connected
        {
            add => _downloadMe.Connected += value;
            remove => _downloadMe.Connected -= value;
        }

        public event Action? Finishing
        {
            add => _downloadMe.Finishing += value;
            remove => _downloadMe.Finishing -= value;
        }

        public event Action? Finished
        {
            add => _downloadMe.Finished += value;
            remove => _downloadMe.Finished -= value;
        }

        public event Action? Paused
        {
            add => _downloadMe.Paused += value;
            remove => _downloadMe.Paused -= value;
        }

        public int TotalBytesToDownload => _downloadMe.TotalBytesToDownload;

        public void StartDownload(CancellationToken cancellationToken, int initialPosition = 0)
        {
            _downloadMe.StartDownload(cancellationToken, initialPosition);
        }
    }
}